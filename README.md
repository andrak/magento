## Description

```

Custom Footer work & styling on Blank theme.

Work being done:

1. install and set up Magento 2.4.5-p1
2. created new theme
3. added google fonts && icomoon 
4. clean up default Magento stuff from footer
5. added custom template and CMS block for footer links
6. layouts && phtml && css work

```

## Magento install

```
1. install and set up Magento on your local as you wish
2. if you are using Warden, .env file is present in root (https://docs.warden.dev/installing.html)
3. in Magento Administration, change theme to "New theme"
4. create CMS block with ID "footer_links"
5. add CMS content provided. with using Page Builder, insert HTML Code element and paste content inside. No additional settings/steps required
```
## CMS block content
```
<div class="column">
    <div data-role="collapsible">
        <h5 class="block-title" data-role="trigger">DB Sports Products</h5>
    </div>
    <div data-role="content">
        <ul>
            <li><a href="#">Custom link</a></li>
            <li><a href="#">Custom link</a></li>
            <li><a href="#">Custom link</a></li>
            <li><a href="#">Custom link</a></li>
            <li><a href="#">Custom link</a></li>
            <li><a href="#">Custom link</a></li>
            <li><a href="#">Custom link</a></li>
            <li><a href="#">Custom link</a></li>
            <li><a href="#">Custom link</a></li>
        </ul>
    </div>
</div>

<div class="column">
    <div data-role="collapsible">
        <h5 class="block-title" data-role="trigger">Customer Service</h5>
    </div>
    <div data-role="content">
        <ul>
            <li><a href="#">Customer Service</a></li>
            <li><a href="#">Customer Service</a></li>
            <li><a href="#">Customer Service</a></li>
            <li><a href="#">Customer Service</a></li>
            <li><a href="#">Customer Service</a></li>
            <li><a href="#">Customer Service</a></li>
            <li><a href="#">Customer Service</a></li>
            <li><a href="#">Customer Service</a></li>
            <li><a href="#">Customer Service</a></li>
            <li><a href="#">Customer Service</a></li>
        </ul>
    </div>
</div>

<div class="column">
    <div data-role="collapsible">
        <h5 class="block-title" data-role="trigger">Company Information </h5>
    </div>
    <div data-role="content">
        <ul>
            <li><a href="#">Company information</a></li>
            <li><a href="#">Company information</a></li>
            <li><a href="#">Company information</a></li>
            <li><a href="#">Company information</a></li>
            <li><a href="#">Company information</a></li>
        </ul>
    </div>
</div>

<div class="column social">
    <div data-role="collapsible">
        <h5 class="block-title" data-role="trigger">Follow us</h5>
    </div>
    <div data-role="content">
        <ul>
            <li><a href="#" class="icon-facebook"><span>Facebook</span></a></li>
            <li><a href="#" class="icon-instagram"><span>Instagram</span></a></li>
            <li><a href="#" class="icon-twitter"><span>Twitter</span></a></li>
        </ul>
    </div>
</div>




```

